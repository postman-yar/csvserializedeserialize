﻿using System.Collections.Generic;
using System.Text;

namespace CSVSerializeDeserialize
{
    public static class SerializeCSV<T>
    {

        public static string Serialize(T obj)
        {
            var sb = new StringBuilder();

            sb.AppendLine(GetHeaderLine(obj));

            sb.AppendLine(GetObjectLine(obj));

            return sb.ToString();
        }

        public static string Serialize(T[] obj)
        {
            var sb = new StringBuilder();

            sb.AppendLine(GetHeaderLine(obj[0]));

            foreach (var ob in obj)
            {
                sb.AppendLine(GetObjectLine(ob));
            }
            return sb.ToString();
        }

        public static string Serialize(List<T> obj)
        {
            var sb = new StringBuilder();

            sb.AppendLine(GetHeaderLine(obj[0]));

            foreach (var ob in obj)
            {
                sb.AppendLine(GetObjectLine(ob));
            }
            return sb.ToString();
        }

        private static string GetHeaderLine(T obj)
        {
            var sb = new StringBuilder();

            var myType = obj.GetType();

            var fields = myType.GetFields();

            foreach (var field in fields)
            {
                _ = sb.Length == 0 ? sb.Append(field.Name) : sb.Append(";" + field.Name);
            }
            return sb.ToString();
        }

        private static string GetObjectLine(T obj)
        {
            var sb = new StringBuilder();

            var myType = obj.GetType();

            var fields = myType.GetFields();

            foreach (var field in fields)
            {
                _ = sb.Length == 0 ? sb.Append(field.GetValue(obj)) : sb.Append(";" + field.GetValue(obj));
            }
            return sb.ToString();
        }
    }
}
