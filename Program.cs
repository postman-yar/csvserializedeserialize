﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace CSVSerializeDeserialize
{

    class Program
    {
        private const int CircleCount = 1_000_000;
        private static void Main(string[] args)
        {
            OneObjectTest();
        }

        private static void OneObjectTest()
        {
            var fooObject = new Foo().Get();

            Console.WriteLine("Тестирование одного элемента");

            //custom serialization
            var startTime = System.Diagnostics.Stopwatch.StartNew();

            var s = "";
            for (var i = 0; i < CircleCount; i++)
            {
                s = SerializeCSV<Foo>.Serialize(fooObject);
            }

            startTime.Stop();
            Console.WriteLine($"CSV Serialized time: {Format.ResultTime(startTime.Elapsed)}");

            //custom deserialization

            startTime = System.Diagnostics.Stopwatch.StartNew();
            for (var i = 0; i < CircleCount; i++)
            {
                _ = DeserializeCSV<Foo>.DeserializeOne(s);
            }

            startTime.Stop();

            Console.WriteLine($"CSV Deserialized time: {Format.ResultTime(startTime.Elapsed)}");

            //Newtonsoft.Json serialization
            startTime = System.Diagnostics.Stopwatch.StartNew();
            for (var i = 0; i < CircleCount; i++)
            {
                s = JsonConvert.SerializeObject(fooObject);
            }

            startTime.Stop();

            Console.WriteLine($"JsonConvert Serialized time: {Format.ResultTime(startTime.Elapsed)}");

            //Newtonsoft.Json deserialization
            startTime = System.Diagnostics.Stopwatch.StartNew();
            for (var i = 0; i < CircleCount; i++)
            {
                _ = JsonConvert.DeserializeObject<Foo>(s);
            }

            startTime.Stop();

            Console.WriteLine($"JsonConvert Deserialized time: {Format.ResultTime(startTime.Elapsed)}");
        }
        private static void ListTest()
        {
            Console.WriteLine("Тестирование списка");

            var fooList = new List<Foo>();
            for (var i = 0; i < CircleCount; i++)
            {
                fooList.Add(new Foo().Get());
            }

            //custom serialization
            var startTime = System.Diagnostics.Stopwatch.StartNew();

            var s = SerializeCSV<Foo>.Serialize(fooList);

            startTime.Stop();

            var serializationTime = startTime.Elapsed;


            startTime = System.Diagnostics.Stopwatch.StartNew();
            Console.WriteLine(s);
            startTime.Stop();

            Console.WriteLine($"Время на вывод в консоль: {Format.ResultTime(startTime.Elapsed)}");

            Console.WriteLine($"CSV Serialized time: {Format.ResultTime(serializationTime)}");

            //custom deserialization
            startTime = System.Diagnostics.Stopwatch.StartNew();

            _ = DeserializeCSV<Foo>.Deserialize(s);

            startTime.Stop();

            Console.WriteLine($"CSV Deserialized time: {Format.ResultTime(startTime.Elapsed)}");

            //Newtonsoft.Json serialization
            startTime = System.Diagnostics.Stopwatch.StartNew();

            var JSONs = JsonConvert.SerializeObject(fooList);

            startTime.Stop();

            Console.WriteLine($"JsonConvert Serialized time: {Format.ResultTime(startTime.Elapsed)}");

            //Newtonsoft.Json deserialization
            startTime = System.Diagnostics.Stopwatch.StartNew();

            _ = JsonConvert.DeserializeObject<List<Foo>>(JSONs);

            startTime.Stop();

            Console.WriteLine($"JsonConvert Deserialized time: {Format.ResultTime(startTime.Elapsed)}");
        }
    }
}
