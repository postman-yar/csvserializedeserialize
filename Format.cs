﻿using System;

namespace CSVSerializeDeserialize
{
    public static class Format
    {
        public static string ResultTime(TimeSpan resTimeSpan)
        {
            return string.Format("{0:00}:{1:00}:{2:00}.{3:000}",
                resTimeSpan.Hours,
                resTimeSpan.Minutes,
                resTimeSpan.Seconds,
                resTimeSpan.Milliseconds);
        }
    }
}
